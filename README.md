# ml-python-predictive-analysis

#### 介绍
《Python机器学习预测分析核心算法》(machine learning in python -- essential techniques for predictive-analysis)代码示例。使用jupyter学习，并使用python3.6+（原书使用的是python2.7+）

#### 模块说明
原书代码：为书籍附录代码  
chart2~chart7：是对书中代码的还原  
data：保存书中用到的数据，避免每次都从远程获取  
output：用以保存书中的一些输出，包括：模型结果、图片等  
scripts：抽离出来的py脚本，如：取数逻辑。但这样放在这里是用不了的，只为练习